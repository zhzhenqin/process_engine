### 流程引擎


### 项目介绍

process-engine


1. 定义业务执行流程，使得实现复杂逻辑代码时，不再面向过程，不再123456789步
2. 新来的伙伴不用再看老代码时，到处跳转，东跳一下，西跳一下看逻辑
3. 实现上下文管控，不再随意往上下文中添加各种参数传递
4. 实现前处理和后处理类切面，可用于但不限于，日志收集，业务数据收集，指标收集等。


### 语言：Java


### 使用说明

无须 Spring 支持，仅使用了 Spring AOP 的部分功能。

```xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-aop</artifactId>
    <version>${spring.version}</version>
    <exclusions>
         <exclusion>
             <groupId>org.springframework</groupId>
             <artifactId>spring-beans</artifactId>
         </exclusion>
    </exclusions>
</dependency>
```

### 二步：定义流程


```java
public static class XProcess extends AbstractProcessDefineRegister {
    private static Handler<Integer, Object> startHandler = new HelloHander("START");
    private static Handler<Integer, Object> oneHandler = new HelloHander("ONE");
    private static Handler<Integer, Object> twoHandler = new HelloHander("TWO");
    private static Handler<Integer, Object> threeHandler = new HelloHander("THREE");
    private static Handler<Integer, Object> fourHandler = new HelloHander("FOUR");

    private static Handler<Integer, Object> endHandler = new HelloHander("END");

    /**
     * 流程定义
     */
    private final ProcessDefine<Integer, Object> process = new DefaultProcessDefine<>();

    @Override
    protected ProcessDefine createProcessDefine() {
        return this.process;
    }

    @Override
    public void register() {
        //定义业务流程第一步
        process.defineStart(startHandler);
        //定义第一步执行结果为1时，执行 oneHandler, 为 2 时执行 two
        process.doHandler(startHandler)
                .when(1).go(oneHandler)
                .when(2).go(twoHandler);
        //定义 two 执行结果为4时，执行 fourHandler, 为 3 时执行 three
        process.doHandler(twoHandler).when(4).go(fourHandler).when(3).go(threeHandler);

        // one 执行完成后无论如何要执行 endHandler
        process.doHandler(oneHandler).always(endHandler);
        // four 执行完成后无论如何要执行 endHandler
        process.doHandler(fourHandler).always(endHandler);
        //重试增强控制器，当执行sixHandler执行结果不为true时，发生重试，每次间隔10秒，重试3次
        //process.doController(new RetryController(twoHandler, true, 10, 3));
    }
}

class HelloHander implements Handler<Integer, Object> {

    String name;

    public HelloHander(String name) {
        this.name = name;
    }

    @Override
    public Shunt<Integer, Object> execute(Context context) {
        int num = new Random().nextInt(5);
        System.out.println("=======================");
        System.out.println("Hello, " + name + " return: " + num);
        System.out.println("=======================");
        return new InjectedShunt<Integer, Object>(num, name);
    }

    @Override
    public String getName() {
        return name;
    }
}

public static void main(String[] args) {
    Map<String,Object> initParam = Maps.newHashMap();
    Context processCtx = ProcessContextFactory.createDefaultProcessCtx(initParam, "test");
    ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2, 10,
               60, TimeUnit.SECONDS,
               new ArrayBlockingQueue<>(300));
    Map<String, AbstractProcessDefineRegister> process = ImmutableMap.of("test", new XProcess());
    DefaultProcessManager defaultProcessManager = new DefaultProcessManager(
               poolExecutor,
               process);
    defaultProcessManager.run(processCtx); 
}
```
以上流程实现 ![test-process](manual/test-process.png)

package cn.ted.process.engine.controller;

import cn.ted.process.engine.Context;
import cn.ted.process.engine.handler.Handler;
import cn.ted.process.engine.handler.Shunt;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 *
 * 并发执行 Action
 *
 *
 * @author: zhaopx
 * @Date: 2018/6/15 0015 17:07
 * @Description:
 */
public class ProcessCallable<Matcher extends Comparable<Matcher>,Entity>
        implements Callable<Shunt<Matcher,Entity>> {
    private final Context<Matcher> context;

    private final Handler<Matcher,Entity> handler;

    /**
     * 计时器
     */
    private final CountDownLatch cd;

    public ProcessCallable(Context<Matcher> context,
                           Handler<Matcher, Entity> handler,
                           CountDownLatch cd) {
        this.context = context;
        this.handler = handler;
        this.cd = cd;
    }

    @Override
    public Shunt<Matcher,Entity> call() throws Exception {
        try {
            return handler.execute(context);
        } finally {
            // 一定要减数
            cd.countDown();
        }
    }
}

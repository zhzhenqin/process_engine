package cn.ted.process.engine.controller;


import cn.ted.process.engine.Context;
import cn.ted.process.engine.handler.Handler;
import cn.ted.process.engine.handler.Shunt;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;


/**
 * @author: Ted
 * @Date: 2018/5/2 0002 15:51
 * @Description:
 */
@Slf4j
public class RetryController<Matcher extends Comparable<Matcher>, Entity>
        implements Controller<Matcher, Entity> {

    private static final Logger      LOGGER    = LoggerFactory.getLogger(RetryController.class);

    private final Handler<Matcher, Entity> handler;

    private final Matcher                  target;

    private long                     timeSpace;

    private long                     times;


    /**
     * 当结果不是 target 时，使任务失败
     */
    private boolean notMatchTargetError = false;



    public RetryController(Handler<Matcher, Entity> handler,
                           Matcher target,
                           long timeSpace,
                           long times) {
        this.handler = handler;
        this.target = target;
        this.timeSpace = timeSpace;
        this.times = times;
        this.notMatchTargetError = false;
    }


    public RetryController(Handler<Matcher, Entity> handler,
                           Matcher target,
                           long timeSpace,
                           long times, boolean notMatchTargetError) {
        this.handler = handler;
        this.target = target;
        this.timeSpace = timeSpace;
        this.times = times;
        this.notMatchTargetError = notMatchTargetError;
    }


    @Override
    public Shunt<Matcher, Entity> execute(Context context) {
        Shunt<Matcher, Entity> shunt = null;
        for (int i = 1; i <= times; i++) {
            if(i > 1) {
                // 重试了第二次以上了，打印日志
                log.warn(handler.getName() + " retry execute times {}", i);
            }
            shunt = handler.execute(context);
            if (target.compareTo(shunt.getMatcher()) == NumberUtils.INTEGER_ZERO) {
                return shunt;
            } else {
                try {
                    Thread.sleep(timeSpace * 1000);
                } catch (InterruptedException e) {
                    log.error("[RetryController try try : ]" + handler.getName() + "has error:{}", e, e.getMessage());
                }
            }
        }
        // 不匹配时失败
        if(notMatchTargetError) {
            throw new IllegalStateException("handler execute return shunt " + shunt + " can't match target.");
        }
        return shunt;
    }

    @Override
    public String getName() {
        return this.getClass().getName();
    }

    public void setTimeSpace(long timeSpace) {
        this.timeSpace = timeSpace;
    }

    public void setTimes(long times) {
        this.times = times;
    }

    public void setNotMatchTargetError(boolean notMatchTargetError) {
        this.notMatchTargetError = notMatchTargetError;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RetryController<?, ?> that = (RetryController<?, ?>) o;
        return Objects.equals(handler, that.handler);
    }

    @Override
    public int hashCode() {
        return Objects.hash(handler);
    }
}

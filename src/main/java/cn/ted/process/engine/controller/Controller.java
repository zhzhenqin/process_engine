package cn.ted.process.engine.controller;


import cn.ted.process.engine.Action;
import cn.ted.process.engine.handler.Shunts;
import cn.ted.process.engine.handler.Shunt;

/**
 * @author Ted(lyc)
 * @version : id: Controller , v 0.1 2017/12/28 20:44 Ted(lyc)
 * @Description
 */
public interface Controller<Matcher extends Comparable<Matcher>, Entity> extends Action<Matcher, Entity> {

    public static final Shunt SUCCESS_ENDING = Shunts.injectedBoolShunt(true);

    public static final Shunt FAILURE_ENDING = Shunts.injectedBoolShunt(false);
}
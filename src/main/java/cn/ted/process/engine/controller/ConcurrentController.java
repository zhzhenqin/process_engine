package cn.ted.process.engine.controller;

import cn.ted.process.engine.Context;
import cn.ted.process.engine.handler.Shunts;
import cn.ted.process.engine.handler.Handler;
import cn.ted.process.engine.handler.Shunt;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 *
 *
 * 并发执行多个任务的控制器（Controller）
 *
 *
 * @author: zhaopx
 * @Date: 2018/6/15 0015 16:26
 * @Description:
 */
public class ConcurrentController<Matcher extends Comparable<Matcher>, Entity>
        implements Controller<Matcher, Entity> {

    /**
     * 线程池
     */
    private final ExecutorService poolExecutor;

    /**
     * 一次性的多个操作
     */
    private final List<Handler<Matcher, Entity>> handlers;


    /**
     * 结果
     */
    private Shunt<Matcher, Entity> result;


    /**
     * 超时时间？单位为秒，大于 0 有效。 0 或者 小于 0 代表不限制
     */
    private long timeout = -1;

    /**
     * Action Name
     */
    private String name;

    public ConcurrentController(ExecutorService poolExecutor,
                                Shunt<Matcher, Entity> result,
                                Handler<Matcher, Entity>... handlers) {
        this.poolExecutor = poolExecutor;
        this.handlers = ImmutableList.copyOf(handlers);
        this.result = result;
    }

    @Override
    public Shunt<Matcher, Entity> execute(Context context) {
        final CountDownLatch cd = new CountDownLatch(handlers.size());
        List<Future<Shunt<Matcher, Entity>>> futures = new ArrayList<>(handlers.size());

        // 并发执行任务
        for (Handler<Matcher, Entity> hander : handlers) {
            futures.add(poolExecutor.submit(new ProcessCallable<Matcher, Entity>(context, hander, cd)));
        }
        if(timeout > 0) {
            try {
                // 所有任务执行完成后，通过
                cd.await(timeout, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                // 不限时间
                cd.await();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        // 合并结果
        List<Shunt<Matcher, Entity>> shuntList = Lists.newArrayList();
        futures.stream().forEach(f -> {
            try {
                shuntList.add(f.get());
            } catch (Exception e) {
            }
        });
        return Shunts.mergeShunts(result, shuntList);
    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name == null ? this.getClass().getName() : this.name;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public ExecutorService getPoolExecutor() {
        return poolExecutor;
    }
}

package cn.ted.process.engine.handler;

import cn.ted.process.engine.Context;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2019/5/15
 * Time: 12:47
 * Vendor: yiidata.com
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
@Slf4j
public class ShellTaskHandler implements Handler<Integer, Object> {


    /**
     * Shell 运行时的工作目录
     *
     */
    final String workDir;


    /**
     * Shell path, 绝对路径
     */
    final String shellPath;


    /**
     * 任务参数
     */
    final List<String> args;


    public ShellTaskHandler(File shellPath, String... args) {
        this(shellPath.getParentFile(), shellPath, Arrays.asList(args));
    }


    /**
     * 工作路径为 shell 的父目录
     * @param shellPath
     */
    public ShellTaskHandler(File shellPath, List<String> args) {
        this(shellPath.getParentFile(), shellPath, args);
    }



    public ShellTaskHandler(File workDir, File shellPath, List<String> args) {
        this.workDir = workDir.getAbsolutePath();
        this.shellPath = shellPath.getAbsolutePath();
        this.args = args;
        log.info("shell path {}", this.shellPath);
    }


    /**
     * 返回退出码，0 为正常，非 0 为异常
     * @param context
     * @return
     */
    @Override
    public Shunt<Integer, Object> execute(Context context) {
        DefaultExecutor executor = new DefaultExecutor();
        executor.setExitValue(0);

        // 把任务日志重定向到当前控制台
        DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
        executor.setStreamHandler(new PumpStreamHandler(System.out));

        // 两个小时内必须执行完
        final ExecuteWatchdog watchDog = new ExecuteWatchdog(72000 * 1000L);
        executor.setWatchdog(watchDog);

        String args = StringUtils.join(this.args, " ");

        // 构造执行路径
        String cmd = "bash " + shellPath + " " + args;
        int exitCode = -1;
        String error;
        try {
            log.info("cd " + workDir);
            executor.setWorkingDirectory(new File(workDir));
            log.info("exec: " + cmd);
            executor.execute(CommandLine.parse(cmd), System.getenv(), handler);
            handler.waitFor();
            exitCode = handler.getExitValue();
            log.info("exec {} return code {}", cmd, exitCode);
            return new InjectedShunt<Integer, Object>(exitCode, "OK");
        } catch (Exception e) {
            log.error("execute " + cmd + " error.", e);
            error = e.getMessage();
        }
        return new InjectedShunt<Integer, Object>(exitCode, error);
    }


    /**
     * 返回 Shell 文件名称
     * @return
     */
    @Override
    public String getName() {
        return new File(shellPath).getName();
    }


    public String getWorkDir() {
        return workDir;
    }

    public String getShellPath() {
        return shellPath;
    }

    public List<String> getArgs() {
        return args;
    }
}

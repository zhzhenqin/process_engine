package cn.ted.process.engine.handler;

import java.util.List;
import java.util.Map;

/**
 * @author Ted(lyc)
 * @version : id: Shunts , v 0.1 2018/1/2 13:31 Ted(lyc)
 */
public final class Shunts {

    /**
     * 创建一个boolean类型做为比较器的分流器
     * @param matcher  true 表示当前action执行成功 false 表示当前action执行失败
     * @return
     */
    public static InjectedShunt injectedBoolShunt(boolean matcher) {
        InjectedShunt shunt = new InjectedShunt<Boolean, Object>(matcher);
        return shunt;
    }

    /**
     * 创建一个Integer类型做为比较器的分流器
     * @param matcher  true 表示当前action执行成功 false 表示当前action执行失败
     * @return
     */
    public static InjectedShunt injectedIntegerShunt(Integer matcher) {
        InjectedShunt shunt = new InjectedShunt<Integer, Object>(matcher);
        return shunt;
    }

    public static <Matcher extends Comparable<Matcher>, Entity> InjectedShunt mergeShunts(Shunt<Matcher,Entity> main,
                                                                                          List<Shunt<Matcher,Entity>> shuntList){
        InjectedShunt injectedShunt = new InjectedShunt<Matcher, Entity>(main.getMatcher());
        for(Shunt<Matcher,Entity> shunt : shuntList){
            for(Map.Entry<Object,Object> entry : shunt.getInjectedParam().entrySet()){
                injectedShunt.addInjectedParam(String.class.cast(entry.getKey()),entry.getValue());
            }
        }
        return injectedShunt;
    }
}
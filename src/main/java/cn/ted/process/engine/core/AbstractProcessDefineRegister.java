package cn.ted.process.engine.core;

import cn.ted.process.engine.Context;
import cn.ted.process.engine.handler.Aspect;
import cn.ted.process.engine.handler.Shunt;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * @author Ted(lyc)
 * @version : id: AbstractProcessDefineRegister , v 0.1 2018/1/4 17:41 Ted(lyc)
 * @Description 抽象流程定义注册
 */
@Slf4j
public abstract class AbstractProcessDefineRegister implements ProcessDefineRegister {

    /**
     * 异步执行线程池
     */
    protected ExecutorService poolExecutor;

    /**
     * 获得流程定义器
     * @return
     */
    protected abstract ProcessDefine createProcessDefine();


    /**
     * 同步提交
     * @param context 流程上下文
     * @return
     * @throws RuntimeException
     */
    @Override
    public final Shunt run(Context context) throws RuntimeException {
        return doProcess(context);
    }


    /**
     * 异步提交任务
     * @param context
     * @return
     */
    @Override
    public Future<?> aSyncRun(Context context) {
        return poolExecutor.submit(() -> {
            try {
                doProcess(context);
            } catch (Exception e) {
                log.error("[异步执行任务] 失败: ", e, e.getMessage());
            }
        });
    }

    private void logPath(LinkedHashMap<ProcessNode, Object> executePath) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<ProcessNode, Object> entry : executePath.entrySet()) {
            sb.append(entry.getKey().getAction().getClass().getName() + " result : " + entry.getValue() + " , ");
        }
        log.info("[AbstractProcessDefineRegister] execute path : " + sb.toString());
    }

    /**
     * 递归执行
     * @param processNode
     * @param context
     * @param executePath 每个节点的执行结果
     * @return
     * @throws IllegalStateException
     */
    private Shunt execute(ProcessNode processNode,
                          Context context,
                          LinkedHashMap<ProcessNode, Object> executePath) throws IllegalStateException {
        Shunt result = null;
        try {
            doBefore(context, processNode);
            result = processNode.getAction().execute(context);
            executePath.put(processNode, result);
            doAfter(context, processNode, result);
            return doNext(processNode, context, result, executePath);
        } catch (Throwable e) {
            log.error("execute " + processNode.getUnitKey() + " error.", e);
            executePath.put(processNode, e);
            return doNext(processNode, context, e, executePath);
        }
    }


    /**
     * 异常执行上一个，执行下一个
     * @param processNode
     * @param context
     * @param e
     * @param executePath
     * @return
     */
    private Shunt doNext(ProcessNode processNode,
                         Context context,
                         Throwable e,
                         LinkedHashMap<ProcessNode, Object> executePath) {
        ProcessNode always = processNode.getNexts().get(null);
        if (always != null) {
            return execute(always, context, executePath);
        }
        for (Class condition : processNode.getExceptionNexts().keySet()) {
            if (e.getClass().isAssignableFrom(condition)) {
                return execute(processNode.getExceptionNexts().get(condition), context, executePath);
            }
        }
        // 无法拦截的异常，直接抛出
        throw new RuntimeException(e);
    }


    /**
     * 正常执行完成后，执行下一个 Node
     * @param processNode
     * @param context
     * @param result
     * @param executePath
     * @return
     */
    private Shunt doNext(ProcessNode processNode,
                         Context context,
                         Shunt result,
                         LinkedHashMap<ProcessNode, Object> executePath) {
        ProcessNode always = processNode.getNexts().get(null);
        if (always != null) {
            return execute(always, context, executePath);
        }
        for (Object condition : processNode.getNexts().keySet()) {
            if (result.shunt(condition) == 0) {
                return execute(processNode.getNexts().get(condition), context, executePath);
            }
        }
        log.info("[AbstractProcessDefineRegister] this action : " + processNode.getAction().getClass().getName() + " is the last one ");
        return result;
    }

    private void doAfter(Context context, ProcessNode node, Shunt result) {
        try {
            if (CollectionUtils.isNotEmpty(node.getAfterAspect())) {
                for (Aspect aspect : node.getAfterAspect()) {
                    aspect.execute(context);
                }
            }
        } catch (Exception ex) {
            log.warn("[AbstractProcessDefineRegister] execute node: " + node.getAction().getName() + " after aspect error,{}", ex, ex.getMessage());
        }
    }

    private void doBefore(Context context, ProcessNode node) {
        try {
            if (CollectionUtils.isNotEmpty(node.getBeforeAspect())) {
                for (Aspect aspect : node.getBeforeAspect()) {
                    aspect.execute(context);
                }
            }
        } catch (Exception ex) {
            log.warn("[AbstractProcessDefineRegister] execute node: " + node.getAction().getName() + " before aspect error,{}", ex, ex.getMessage());
        }
    }

    private Shunt doProcess(Context context) {
        ProcessDefine processDefine = createProcessDefine();
        if (processDefine == null) {
            throw new IllegalStateException("ProcessDefine can not be null!");
        }
        if (context == null) {
            throw new IllegalStateException("Context can not be null!");
        }
        ProcessNode root = processDefine.getProcessStore().getRootNode();
        if (root == null) {
            throw new IllegalStateException("DefineStartAction can not be null!");
        }

        LinkedHashMap<ProcessNode, Object> executePath = Maps.newLinkedHashMap();

        Shunt shunt = execute(root, context, executePath);
        logPath(executePath);
        return shunt;
    }


    public void setPoolExecutor(ExecutorService poolExecutor) {
        this.poolExecutor = poolExecutor;
    }
}
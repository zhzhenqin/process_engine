package cn.ted.process.engine.core;

import cn.ted.process.engine.utils.AopTargetUtils;
import cn.ted.process.engine.Context;
import cn.ted.process.engine.handler.Shunt;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author Ted(lyc)
 * @version : id: ProcessManagerImpl , v 0.1 2018/1/3 18:11 Ted(lyc)
 * @Description  流程管理器
 */
@Slf4j
public class DefaultProcessManager implements ProcessDefineRegister {

    /** 所有的流程 */
    private final Map<String, AbstractProcessDefineRegister> processDefineMap = Maps.newConcurrentMap();

    /**
     * 异步执行线程池
     */
    final ExecutorService poolExecutor;


    final Map<String, AbstractProcessDefineRegister> process;


    public DefaultProcessManager(ExecutorService poolExecutor,
                                 Map<String, AbstractProcessDefineRegister> process) {
        this.poolExecutor = poolExecutor;
        this.process = process;
        // 注册所有的流程图
        register();
    }

    @Override
    public void register() {
        if (MapUtils.isEmpty(process)) {
            throw new IllegalStateException("Cannot register ProcessDefine, please make sure the ProcessDefine setup as spring bean");
        }
        for (Map.Entry<String, AbstractProcessDefineRegister> entry : process.entrySet()) {
            AbstractProcessDefineRegister processDefineRegisterProxy = (AbstractProcessDefineRegister) AopTargetUtils.getTarget(entry.getValue());
            String processScene = entry.getKey();

            AbstractProcessDefineRegister pd = this.processDefineMap.get(processScene);
            if (pd != null) {
                throw new IllegalStateException(
                    "Cannot register processDefine, the processDefine name has been register by other processDefine Map, processDefine: " + pd.getClass().getName());
            }
            try {
                processDefineRegisterProxy.register();
            } catch (Exception e) {
                throw new IllegalStateException(
                    "Cannot init processDefine, the processDefine has exception when init, processDefine: " + processDefineRegisterProxy.getClass().getName());
            }

            this.processDefineMap.put(processScene, processDefineRegisterProxy);
            log.info("Register ProcessDefine: {} - {}", processScene, processDefineRegisterProxy.getClass().getName());
        }
    }

    @Override
    public Shunt run(Context context) {
        AbstractProcessDefineRegister process = this.processDefineMap.get(context.getName());
        if (process == null) {
            throw new IllegalArgumentException("the ProcessDefineRegister can not be found by name : " + context.getName());
        }
        // 设置异步线程池
        process.setPoolExecutor(this.poolExecutor);
        try {
            return process.run(context);
        } catch (Exception e) {
            log.error("execute process " + context.getName() + " error.",e);
            throw e;
        }
    }


    /**
     * 异步执行
     * @param context
     * @return
     */
    @Override
    public Future<?> aSyncRun(Context context) {
        return poolExecutor.submit(() -> run(context));
    }
}
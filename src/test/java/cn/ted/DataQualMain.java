package cn.ted;

import cn.ted.process.engine.Context;
import cn.ted.process.engine.ProcessContextFactory;
import cn.ted.process.engine.core.AbstractProcessDefineRegister;
import cn.ted.process.engine.core.DefaultProcessDefine;
import cn.ted.process.engine.core.DefaultProcessManager;
import cn.ted.process.engine.core.ProcessDefine;
import cn.ted.process.engine.handler.ShellTaskHandler;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Unit test for simple App.
 */
public class DataQualMain extends AbstractProcessDefineRegister {

    /**
     * 流程定义
     */
    private final ProcessDefine<Integer, Object> process = new DefaultProcessDefine<>();

    @Override
    protected ProcessDefine createProcessDefine() {
        return this.process;
    }

    @Override
    public void register() {
        ShellTaskHandler taskHandler = new ShellTaskHandler(
                new File("/Users/zhenqin/temp/process_engine/manual/shell/randmcode.sh"), "1");

        ShellTaskHandler taskHandler2 = new ShellTaskHandler(
                new File("/Users/zhenqin/temp/process_engine/manual/shell/echohello.sh"), "2");

        ShellTaskHandler taskHandler3 = new ShellTaskHandler(
                new File("/Users/zhenqin/temp/process_engine/manual/shell/echohello.sh"), "3");
        // 当 taskHandler 执行的返回结果不是 1 时，等待 10s 后再次执行 3 次。
        // RetryController controller = new RetryController(taskHandler, 1, 10, 3);
        //定义业务流程第一步
        process.defineStart(taskHandler);
        // 选择执行
        process.doHandler(taskHandler)
                .when(0).go(taskHandler2)
                .when(1).go(taskHandler3);

        // 顺序执行
        //process.doHandler(taskHandler).always(taskHandler2);
        //process.doHandler(taskHandler2).always(taskHandler3);
    }

    public static void main(String[] args) {
        Map<String,Object> initParam = Maps.newHashMap();

        //app name, 内部通过该值获取注册的 handler
        String processName = "qual_app";
        Context processCtx = ProcessContextFactory.createDefaultProcessCtx(initParam, processName);

        // 提交任务所用的线程池，设置一个合理的值
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2, 10,
                60, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(300));

        Map<String, AbstractProcessDefineRegister> process = ImmutableMap.of(processName,
                new DataQualMain());
        DefaultProcessManager defaultProcessManager = new DefaultProcessManager(
                poolExecutor,
                process);
        defaultProcessManager.run(processCtx);
    }
}

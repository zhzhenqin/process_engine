package cn.ted;

import cn.ted.process.engine.Context;
import cn.ted.process.engine.ProcessContextFactory;
import cn.ted.process.engine.controller.RetryController;
import cn.ted.process.engine.core.AbstractProcessDefineRegister;
import cn.ted.process.engine.core.DefaultProcessDefine;
import cn.ted.process.engine.core.DefaultProcessManager;
import cn.ted.process.engine.core.ProcessDefine;
import cn.ted.process.engine.handler.Handler;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2019/10/11
 * Time: 17:49
 * Vendor: yiidata.com
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class RetryTest extends AbstractProcessDefineRegister {

    /**
     * 流程定义
     */
    private final ProcessDefine<Integer, Object> process = new DefaultProcessDefine<>();

    @Override
    protected ProcessDefine createProcessDefine() {
        return this.process;
    }

    @Override
    public void register() {
        Handler<Integer, Object> startHandler = new HelloHander("START");
        Handler<Integer, Object> oneHandler = new HelloHander("RETRY");

        Handler<Integer, Object> endHandler = new HelloHander("END");

        //定义业务流程第一步
        process.defineStart(startHandler);
        //重试增强控制器，当执行 sixHandler 执行结果不为true时，发生重试，每次间隔10秒，重试3次
        RetryController<Integer, Object> action = new RetryController<>(oneHandler, 1, 10, 3);
        //action.setNotMatchTargetError(true);
        process.doHandler(startHandler).go(action);
        process.doController(action).go(endHandler);
        //process.doHandler(oneHandler).always(endHandler);
    }


    public static void main(String[] args) {
        String app = "retrytest";
        Map<String,Object> initParam = Maps.newHashMap();
        Context processCtx = ProcessContextFactory.createDefaultProcessCtx(initParam, app);

        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2, 10,
                60, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(300));

        Map<String, AbstractProcessDefineRegister> process = ImmutableMap.of(
                app,
                new RetryTest());
        DefaultProcessManager defaultProcessManager = new DefaultProcessManager(
                poolExecutor,
                process);
        defaultProcessManager.run(processCtx);
    }
}
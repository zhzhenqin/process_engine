package cn.ted;

import cn.ted.process.engine.core.AbstractProcessDefineRegister;
import cn.ted.process.engine.core.DefaultProcessDefine;
import cn.ted.process.engine.core.ProcessDefine;
import cn.ted.process.engine.handler.Handler;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2019/5/13
 * Time: 15:20
 * Vendor: yiidata.com
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */

public class XProcess extends AbstractProcessDefineRegister {

    private static Handler<Integer, Object> startHandler = new HelloHander("START");
    private static Handler<Integer, Object> oneHandler = new HelloHander("ONE");
    private static Handler<Integer, Object> twoHandler = new HelloHander("TWO");
    private static Handler<Integer, Object> threeHandler = new HelloHander("THREE");
    private static Handler<Integer, Object> fourHandler = new HelloHander("FOUR");

    private static Handler<Integer, Object> endHandler = new HelloHander("END");


    /**
     * 流程定义
     */
    private final ProcessDefine<Integer, Object> process = new DefaultProcessDefine<>();

    @Override
    protected ProcessDefine createProcessDefine() {
        return this.process;
    }

    @Override
    public void register() {
        //定义业务流程第一步
        process.defineStart(startHandler);
        //定义第一步执行结果为1时，执行 oneHandler, 为 2 时执行 two
        process.doHandler(startHandler)
                .when(1).go(oneHandler)
                .when(2).go(twoHandler);
        //定义 two 执行结果为4时，执行 fourHandler, 为 3 时执行 three
        process.doHandler(twoHandler).when(4).go(fourHandler).when(3).go(threeHandler);

        // one 执行完成后无论如何要执行 endHandler
        process.doHandler(oneHandler).always(endHandler);
        // four 执行完成后无论如何要执行 endHandler
        process.doHandler(fourHandler).always(endHandler);
        //重试增强控制器，当执行sixHandler执行结果不为true时，发生重试，每次间隔10秒，重试3次
        //process.doController(new RetryController(twoHandler, true, 10, 3));
    }
}
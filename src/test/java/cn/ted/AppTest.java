package cn.ted;

import cn.ted.process.engine.Context;
import cn.ted.process.engine.ProcessContextFactory;
import cn.ted.process.engine.core.AbstractProcessDefineRegister;
import cn.ted.process.engine.core.DefaultProcessManager;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Unit test for simple App.
 */
public class AppTest {



    public static void main(String[] args) {
        Map<String,Object> initParam = Maps.newHashMap();
        Context processCtx = ProcessContextFactory.createDefaultProcessCtx(initParam, "apptest");

        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2, 10,
                60, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(300));

        Map<String, AbstractProcessDefineRegister> process = ImmutableMap.of("apptest", new XProcess());
        DefaultProcessManager defaultProcessManager = new DefaultProcessManager(
                poolExecutor,
                process);
        defaultProcessManager.run(processCtx);
    }
}

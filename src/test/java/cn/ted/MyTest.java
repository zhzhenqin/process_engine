package cn.ted;

import cn.ted.process.engine.Context;
import cn.ted.process.engine.ProcessContextFactory;
import cn.ted.process.engine.core.AbstractProcessDefineRegister;
import cn.ted.process.engine.core.DefaultProcessManager;
import cn.ted.process.engine.core.ProcessDefineRegister;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2019/5/13
 * Time: 13:52
 * Vendor: yiidata.com
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class MyTest {

    public static void main(String[] args) {
        Map<String,Object> initParam = Maps.newHashMap();
        Context processCtx = ProcessContextFactory.createDefaultProcessCtx(initParam, "test");

        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2, 10,
                60, TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(300));

        Map<String, AbstractProcessDefineRegister> process = ImmutableMap.of("test", new XProcess());
        DefaultProcessManager defaultProcessManager = new DefaultProcessManager(
                poolExecutor,
                process);
        defaultProcessManager.run(processCtx);

    }
}

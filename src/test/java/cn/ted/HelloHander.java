package cn.ted;

import cn.ted.process.engine.Context;
import cn.ted.process.engine.handler.Handler;
import cn.ted.process.engine.handler.InjectedShunt;
import cn.ted.process.engine.handler.Shunt;

import java.util.Random;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2019/10/11
 * Time: 17:48
 * Vendor: yiidata.com
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class HelloHander implements Handler<Integer, Object> {

    String name;


    public HelloHander(String name) {
        this.name = name;
    }

    @Override
    public Shunt<Integer, Object> execute(Context context) {
        int num = new Random().nextInt(5);
        System.out.println("=======================");
        System.out.println("Hello, " + name + " return: " + num);
        System.out.println("=======================");
        return new InjectedShunt<Integer, Object>(num, name);
    }

    @Override
    public String getName() {
        return name;
    }
}
